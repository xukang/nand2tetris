#ifndef PARSER_H
#define PARSER_H

using namespace std;

enum CommandType {A_COMMAND, C_COMMAND, L_COMMAND};

clas Parser
{
	public:

    /* Opens the input file and get ready to parse it */
	Parser(string* input_file); 

    /* Are there more commands in the input? */
	bool hasMoreCommands();

    /* Reads the next command from the inputs and makes it the current command.
       Should be called only if hasMoreCommands() is true.
       Initially there is no current command. */
	void advance();

    /* Returns the type of the current command:
       A_COMMAND for @Xxx where Xxx is either a symbol or a decimal number
       
    CommandType commandType();

		
}

#endif // PARSER_H
