// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.


@LOOP
0;JMP

(LOOP)
    // Initialize the pixel RAM location in R0
    @SCREEN
    D=A
    @0
    M=D
    
    // Read the KBD
    @KBD
    D=M

    // If KBD is pressed, jump to FILLBLACK
    @FILLBLACK
    D;JGT

    // If KBD is not pressed, jump to FILLWHITE
    @FILLWHITE
    D;JEQ

(FILLBLACK)
    // Fill the pixel black
    @0
    D=M      // The pixel RAM that R0 points to
    A=D      // So that can access the pixel RAM from M
    M=-1     // Set to black

    // Add 1 to p
    @0
    M=M+1

    // If the pixel RAM location is less than KBD, write again
    @0
    D=M
    @KBD
    D=A-D
    @FILLBLACK
    D;JGT

    // Else jump to LOOP
    @LOOP
    0;JMP

(FILLWHITE)
    // Fill the pixel white
    @0
    D=M      // The pixel RAM that p points to
    A=D      // So that can access the pixel RAM from M
    M=0      // Set to white

    // Add 1 to p
    @0
    M=M+1

    // If the pixel RAM location is less than LBD, write again
    @0
    D=M
    @KBD
    D=A-D
    @FILLWHITE
    D;JGT

    // Else jump to LOOP
    @LOOP
    0;JMP


