// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)
//
// This program only needs to handle arguments that satisfy
// R0 >= 0, R1 >= 0, and R0*R1 < 32768.

// Put your code here.


// Set R2 to 0
@R2
M=0

// If R0 > 0, goto LOOP
@R0
D=M
@LOOP
D;JGT

// Else goto END
@END
0;JMP

// Define LOOP, add R1 to R2; 
// Remove 1 from R0;
// If R0 > 0, LOOP again
(LOOP)
    // Get R2
    @R2
    D=M
    
    // Add R1 to R2
    @R1
    D=D+M
    @R2
    M=D

    // Remove 1 from R0
    @R0
    M=M-1
    D=M

    // If R0 still > 0, goto LOOP
    @LOOP
    D;JGT

(END)
    @END
    0;JMP



